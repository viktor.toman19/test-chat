import {Component, OnInit} from '@angular/core';
import {Chat} from "./classes/chat";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    chatMessages: Chat[] = [];
    newMessage = '';

    ngOnInit(): void {

    }

    sendMessage() {
        if (this.newMessage.trim() === '') {
            return;
        }

        try {
            this.chatMessages.push({
                message: this.newMessage,
                userImage: "/assets/images/Ellipse.svg"
            });

            this.newMessage = '';
        } catch (err) {
            console.log(err);
        }
    }
}
